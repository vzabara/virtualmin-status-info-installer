sub script_compoza_status_info_desc
{
    return "Compoza.NET Status API Server";
}

sub script_compoza_status_info_depends
{
    `which openssl` || return 'openssl is needed';
    `which sqlite3` || return 'sqlite3 is needed';
    `which python` || return 'python is needed';
    `which pip` || return 'pip is needed';
    return undef;
}

sub script_compoza_status_info_longdesc
{
    return "Check your user has access rights to Compoza.Net Info API Git repository";
}

sub script_compoza_status_info_versions
{
    return ( "2.0" );
}

sub script_compoza_status_info_category
{
    return "Network Tools";
}

sub script_compoza_status_info_files
{
    local ($d, $ver, $opts, $upgrade) = @_;
    local @files = ();
    return @files;
}

sub script_compoza_status_info_params
{
    local ($d, $ver, $upgrade) = @_;
    my $rv;
    if (!$upgrade) {
        # Show editable install options
        $rv .= ui_table_row("Title",
            ui_textbox("apititle", $d->{'owner'}));
        $rv .= ui_table_row("API port",
            ui_textbox("apiport", 5000));
    }
    return $rv;
}

sub script_compoza_status_info_parse
{
    local ($d, $ver, $in, $upgrade) = @_;
    if ($upgrade) {
        # Options are always the same
        return $upgrade->{'opts'};
    }
    else {
        return {
            'apititle' => $in->{'apititle'},
            'apiport' => $in->{'apiport'},
            'path' => '/compoza_status_info',
        };
    }
}

sub script_compoza_status_info_check
{
    local ($d, $ver, $opts, $upgrade) = @_;
    local ($dom, @doms);

    @doms = &list_domains();
    foreach my $dom (@doms) {
        foreach my $sinfo (&list_domain_scripts($dom)) {
            if ($sinfo->{'name'} eq 'compoza_status_info') {
                return "Compoza.NET Status API Server is already installed under <a href='/virtual-server/list_scripts.cgi?dom=$dom->{'id'}&xnavigation=1'>$dom->{'dom'}</a>";
            }
        }
    }
    !-d "/usr/local/bin/compoza.net.info" || return "Compoza.NET Status API Server appears to be already installed: /usr/local/bin/compoza.net.info";
    return undef;
}

sub script_compoza_status_info_install
{
    local ($d, $version, $opts, $files, $upgrade) = @_;
    local ($out, $ex, $title, $port, $install_dir, $l);

    $install_dir = "/usr/local/bin/compoza.net.info";
    &$first_print('Downloading Compoza.NET Status API Server from Git...');
    # root user has git access key b/c we want to pull changes from Git
    &system_logged("git clone git\@bitbucket.org:vzabara/compoza.net-info-api.git $install_dir");
    -f "$install_dir/main.py" || return (0, "User <tt>\"$d->{'user'}\"</tt> cannot access Compoza.Net Info API Git repository.");

    local $cfile = "$install_dir/config.ini";
    local $lref = &read_file_lines($cfile);
    $title = $opts->{'apititle'};
    if ($title eq "") {
        $title = $d->{'owner'};
        if ($title eq "") {
            $title = $d->{'dom'};
        }
    }
    $port = $opts->{'apiport'};
    if ($port eq "") {
        $port = 5000;
    }
    my $protocol = &domain_has_ssl($d) ? 'https' : 'http';
    foreach $l (@$lref) {
        if ($l =~ /^title\s?=/) {
            $l = "title = $title";
        }
        if ($l =~ /^url\s?=/) {
            $l = "url = $protocol://$d->{'dom'}:$port";
        }
        if ($l =~ /^token\s?=/) {
            $l = "token = " . `openssl rand -hex 80`;
        }
    }
    &flush_file_lines($cfile);
    &system_logged("ln -s ".quotemeta("$install_dir/compozanet-info-firebase-adminsdk-42dgn-e107bb3266.json")." ".
        quotemeta("$install_dir/serviceAccountKey.json"));
    &$second_print('...done');

    &create_system_scripts($d, $port);
    local $url = &script_path_url($d, $opts);

    return (1,
        "<br>Compoza.NET Status API Server installation complete.<br>",
        "Installed under $install_dir",
        $url);
}

sub script_compoza_status_info_uninstall
{
    my ($d, $version, $opts) = @_;
    drop_system_scripts();
    &system_logged("rm -rf /usr/local/bin/compoza.net.info");

    return (1, "Compoza.NET Status API Server directory has been deleted.");
}

sub script_compoza_status_info_site
{
    return "https://static.rompos.com/compoza-info";
}

sub create_system_scripts
{
    local ($d, $port) = @_;
    &$first_print('Installing system scripts...');
    system('pip install fastapi configparser uvicorn[standard]');
    my $uvicorn = `which uvicorn`;
    chomp($uvicorn);
    my $cert = &domain_has_ssl($d) ?
        "--ssl-keyfile=$d->{home}/ssl.key --ssl-certfile=$d->{home}/ssl.cert --ssl-ca-certs=$d->{home}/ssl.ca" :
        "";
    my $str = <<END_SCRIPT;
[Unit]
Description=Status Info Systemd Service
After=network.target
StartLimitIntervalSec=0

[Service]
Type=simple
ExecStart=$uvicorn main:app --host 0.0.0.0 --port $port $cert
User=root
Group=root
WorkingDirectory=/usr/local/bin/compoza.net.info
ExecReload=/bin/kill -s HUP \$MAINPID
KillMode=mixed
TimeoutStopSec=5
PrivateTmp=true
RestartSec=10
Restart=always

[Install]
WantedBy=multi-user.target
END_SCRIPT

    open(FH, '>', "/etc/systemd/system/status-info.service") or die $!;
    print FH $str;
    close(FH);

    &system_logged('systemctl daemon-reload');
    &system_logged("systemctl start status-info.service");
    &system_logged("systemctl enable status-info.service");

    &foreign_require("cron");
    my $job1;
    my $cron_cmd1 = "/usr/local/bin/compoza.net.info/check-services.py --server-id=nfh9m6GLqw13UcN30PVH >> /usr/local/bin/compoza.net.info/check-services-log.txt 2>&1";
    foreach my $j (&cron::list_cron_jobs()) {
        $job1 = $j if ($j->{'command'} eq $cron_cmd1);
    }
    if (!$job1) {
        local $njob = { 'user' => $d->{'user'}, 'active' => 1,
            'mins' => '*', 'hours' => '*', 'days' => '*',
            'months' => '*', 'weekdays' => '*',
            'command' => $cron_cmd1 };
        &lock_file(&cron::cron_file($njob));
        &cron::create_cron_job($njob);
        &unlock_file(&cron::cron_file($njob));
    }
    my $cron_cmd2 = "/usr/local/bin/compoza.net.info/check-updates.py --server-id=nfh9m6GLqw13UcN30PVH >> /usr/local/bin/compoza.net.info/check-updates-log.txt 2>&1";
    my $job2;
    foreach my $j (&cron::list_cron_jobs()) {
        $job2 = $j if ($j->{'command'} eq $cron_cmd2);
    }
    if (!$job2) {
        local $njob = { 'user' => $d->{'user'}, 'active' => 1,
            'mins' => '*', 'hours' => '*', 'days' => '*',
            'months' => '*', 'weekdays' => '*',
            'command' => $cron_cmd2 };
        &lock_file(&cron::cron_file($njob));
        &cron::create_cron_job($njob);
        &unlock_file(&cron::cron_file($njob));
    }
    &$second_print('...done');
}

sub drop_system_scripts
{
    &$first_print('Dropping system scripts...');
    &system_logged("systemctl stop status-info.service");
    &system_logged("systemctl disable status-info.service");
    unlink("/etc/systemd/system/status-info.service");

    &foreign_require("cron");
    my $job1;
    my $cron_cmd1 = "/usr/local/bin/compoza.net.info/check-services.py --server-id=nfh9m6GLqw13UcN30PVH >> /usr/local/bin/compoza.net.info/check-services-log.txt 2>&1";
    foreach my $j (&cron::list_cron_jobs()) {
        $job1 = $j if ($j->{'command'} eq $cron_cmd1);
    }
    if ($job1) {
        &lock_file(&cron::cron_file($job1));
        &cron::delete_cron_job($job1);
        &unlock_file(&cron::cron_file($job1));
    }
    my $job2;
    my $cron_cmd2 = "/usr/local/bin/compoza.net.info/check-updates.py.py --server-id=nfh9m6GLqw13UcN30PVH >> /usr/local/bin/compoza.net.info/check-updates-log.txt 2>&1";
    foreach my $j (&cron::list_cron_jobs()) {
        $job2 = $j if ($j->{'command'} eq $cron_cmd2);
    }
    if ($job2) {
        &lock_file(&cron::cron_file($job2));
        &cron::delete_cron_job($job2);
        &unlock_file(&cron::cron_file($job2));
    }
    &$second_print('...done');
}

1;
